kubectl create ns $1
echo "Namespace is created"
helm install postgres  stable/postgresql --namespace=$1 --set postgresqlPassword=amritaraj --wait
echo "Postgres instance is deployed"
echo "Copying files inside the pod to load data"
kubectl cp postgres.sql $1/postgres-postgresql-0:/tmp/
kubectl cp titanic.csv $1/postgres-postgresql-0:/tmp/
echo "Files are copied"
kubectl exec -it svc/postgres-postgresql -n $1 -- bash -c 'PGPASSWORD=${POSTGRES_PASSWORD} psql -U $POSTGRES_USER -f /tmp/postgres.sql'
echo "Data is loaded successfully in postgres database"
echo "Deploying the api"
kubectl apply -f ./api_deployment_service.yaml -n $1
echo "API is deployed"
#kubectl port-forward -n cs1 svc/api 5002
echo "Now run the command -- kubectl port-forward -n namespacename svc/api 5002 -- to access the api using localhost:5002 "