create extension if not exists "uuid-ossp";

create table people
(
uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
Survived boolean,
Pclass integer,
Name character varying(200),
Sex character varying(10),
Age float(3),
SiblingsOrSpousesAboard integer,
ParentsOrChildrenAboard integer,
Fare float(4),
CONSTRAINT people_pkey PRIMARY KEY (uuid)
);

COPY people(Survived,Pclass,Name,Sex,Age,SiblingsorSpousesAboard,ParentsorChildrenAboard,Fare) FROM '/tmp/titanic.csv' DELIMITER ',' CSV HEADER;
