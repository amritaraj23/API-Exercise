import psycopg2
import psycopg2.extras
from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
import json
from flask import Flask, request, jsonify
import os

try:
    postgresPort=int(os.environ['POSTGRES_PORT'])
    postgresHost=os.environ['POSTGRES_HOST']
    postgresPassword=os.environ['POSTGRES_PASSWORD']
except KeyError as e: 
    print("All Environment Variable not set:Please set environment variable ",e)
    exit(1)

def createpostgresconnection(postgresHost,postgresPassword,postgresPort):
    try:
        connection = psycopg2.connect(
            host=postgresHost,
            port=postgresPort,
            user='postgres',
            password=postgresPassword)
        return connection  
    except Exception as err: 
        raise Exception('Exception Could not establish connection to Postgres Database::',err) from None

app = Flask(__name__)
api = Api(app)

class AllPeople(Resource):
    def get(self):
        connection = createpostgresconnection(postgresHost,postgresPassword,postgresPort)
        cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            cursor.execute("select * from people") # This line performs query and returns json result
            print(cursor)
            ans =cursor.fetchall()
            connection.close()
            ans1 = []
            for row in ans:
                ans1.append(dict(row))
            return ans1
        except Exception as err: 
            raise Exception('Exception Generated ::',err) from None
    def post(self):
        connection = createpostgresconnection(postgresHost,postgresPassword,postgresPort)
        cursor = connection.cursor()
        print(request.json)
        try:
            survived = request.json['survived']
            pclass = request.json['pclass']
            name = request.json['name']
            sex = request.json['sex']
            age = request.json['age']
            siblingsorspousesaboard = request.json['siblingsorspousesaboard']
            parentsorchildrenaboard = request.json['parentsorchildrenaboard']
            fare = request.json['fare']
        except Exception as err:
            raise Exception('Exception Generated Please check the json request::',err) from None
        try:
            sql = "insert into people (survived, pclass, name, sex, age, siblingsorspousesaboard, parentsorchildrenaboard, fare) values("+str(survived)+"," +str(pclass)+",'"+name+"','"+sex+"',"+str(age)+","+str(siblingsorspousesaboard)+","+str(parentsorchildrenaboard)+","+str(fare)+")"
            print(sql)
            cursor.execute(sql)
            connection.commit()
            connection.close()
            return {'status':'success'}
        except Exception as err: 
            raise Exception('Exception Generated ::',err) from None

class People(Resource):
    def get(self, uuid):
        connection = createpostgresconnection(postgresHost,postgresPassword,postgresPort)
        cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            cursor.execute("select * from people where uuid='"+uuid+"'") # This line performs query and returns json result
            print(cursor)
            ans =cursor.fetchone()
            connection.close()
            ans1 = dict(ans)
            return ans1
        except Exception as err: 
            raise Exception('Exception Generated ::',err) from None
    def delete(self, uuid):
        connection = createpostgresconnection(postgresHost,postgresPassword,postgresPort)
        cursor = connection.cursor()
        try:
            sql = "delete from people where uuid='"+uuid+"'"
            print(sql)
            cursor.execute(sql)
            connection.commit()
            connection.close()
            return {'status':'success'}
        except Exception as err: 
            raise Exception('Exception Generated ::',err) from None
    def put(self, uuid):
        connection = createpostgresconnection(postgresHost,postgresPassword,postgresPort)
        cursor = connection.cursor()
        print(request.json)
        try:
            survived = request.json['survived']
            pclass = request.json['pclass']
            name = request.json['name']
            sex = request.json['sex']
            age = request.json['age']
            siblingsorspousesaboard = request.json['siblingsorspousesaboard']
            parentsorchildrenaboard = request.json['parentsorchildrenaboard']
            fare = request.json['fare']
        except Exception as err:
            raise Exception('Exception Generated Please check the json request::',err) from None
        try:
            sql = "update people set survived="+str(survived)+",pclass="+str(pclass)+",name='"+name+"',sex='"+sex+"',age="+str(age)+",siblingsorspousesaboard="+str(siblingsorspousesaboard)+",parentsorchildrenaboard="+str(parentsorchildrenaboard)+",fare="+str(fare)+" where uuid='"+uuid+"'"
            cursor.execute(sql)
            connection.commit()
            connection.close()
            return {'status':'success'}
        except Exception as err: 
            raise Exception('Exception Generated ::',err) from None

api.add_resource(AllPeople, '/people')
api.add_resource(People, '/people/<uuid>')

if __name__ == '__main__':
     app.run(host='0.0.0.0',port='5002')
